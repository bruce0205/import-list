module import-list

go 1.16

require (
	github.com/gocarina/gocsv v0.0.0-20220310154401-d4df709ca055 // indirect
	golang.org/x/text v0.3.7 // indirect
)
