package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/gocarina/gocsv"
	"golang.org/x/text/encoding/traditionalchinese"
	"golang.org/x/text/transform"
)

type Record struct {
	Name        string `csv:"name"`
	Type        string `csv:"type"`
	VarietyEn   string `csv:"variety_en"`
	VarietyZh   string `csv:"variety_zh"`
	Id          string `csv:"id"`
	Gender      string `csv:"gender"`
	Birthday    string `csv:"birthday"`
	Host        string `csv:"host"`
	Phone       string `csv:"phone"`
	Address     string `csv:"address"`
	CityPhone   string `csv:"city_phone"`
	Description string `csv:"description"`
}

func distinctList(inputs []string) []string {
	keys := make(map[string]bool)
	list := []string{}

	for _, input := range inputs {
		if _, value := keys[input]; !value {
			keys[input] = true
			list = append(list, input)
		}
	}
	return list
}

func removeRtfSyntax(content string, replaceFrom []string, replaceTo string) string {
	for _, from := range replaceFrom {
		content = strings.Replace(content, from, replaceTo, 1)
	}
	return content
}

func transformEncoding(rawReader io.Reader, trans transform.Transformer) (string, error) {
	ret, err := ioutil.ReadAll(transform.NewReader(rawReader, trans))
	if err == nil {
		return string(ret), nil
	} else {
		return "", err
	}
}

func main() {
	// action("ok")
	action("fail")
}

func action(fileType string) {
	re := regexp.MustCompile(`\{\*?\\[^{}]+}|[{}]|\\\n?[A-Za-z]+\n?(?:-?\d+)?[ ]?`)
	recordsFile, err := os.OpenFile("records.csv."+fileType, os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer recordsFile.Close()
	records := []*Record{}

	if err := gocsv.UnmarshalFile(recordsFile, &records); err != nil {
		panic(err)
	}
	for _, record := range records {
		// fmt.Println(record.Description)
		results := re.FindAllString(record.Description, -1)

		step1 := record.Description
		step2 := removeRtfSyntax(record.Description, results, "")
		step3 := strings.ReplaceAll(removeRtfSyntax(record.Description, results, ""), "\\'", "\\x")

		fmt.Printf("step1:\n %s\n", step1)
		fmt.Printf("step2:\n %s\n", step2)
		fmt.Printf("step3:\n %s\n", step3)

		content, _ := strconv.Unquote(`"` + fmt.Sprintf("%s", step3) + `"`)
		decoedStr, _, err := transform.String(traditionalchinese.Big5.NewDecoder(), fmt.Sprintf("%s", content))
		fmt.Printf("decoedStr: %s\n", decoedStr)
		fmt.Println(err)
		// break
	}
}

// /\{\*?\\[^{}]+}|[{}]|\\\n?[A-Za-z]+\n?(?:-?\d+)?[ ]?/gm
func testOrigin() {
	file, err := os.Open("record-data.csv")
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer file.Close()
	reader := csv.NewReader(file)

	re := regexp.MustCompile(`\{\*?\\[^{}]+}|[{}]|\\\n?[A-Za-z]+\n?(?:-?\d+)?[ ]?`)
	var count = 0
	typeKeys := make(map[string]bool)
	typeList := []string{}
	varietyKeys := make(map[string]bool)
	varietyList := []string{}

	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Println("Error:", err)
			return
		}
		count = count + 1
		if _, value := typeKeys[record[1]]; !value {
			typeKeys[record[1]] = true
			typeList = append(typeList, record[1])
		}
		if _, value := varietyKeys[record[1]+":"+record[2]]; !value {
			varietyKeys[record[1]+":"+record[2]] = true
			varietyList = append(varietyList, record[1]+":"+record[2])
		}
		fmt.Printf("no: %d, name: %s\n", count, record[0]) // record has the type []string

		results := re.FindAllString(record[11], -1)
		for index, result := range results {
			fmt.Printf("no %d: %s\n", index+1, result)
		}

		s1 := record[11]
		s2 := removeRtfSyntax(record[11], results, "")
		s3 := strings.ReplaceAll(removeRtfSyntax(record[11], results, ""), "\\'", "\\x")
		s4 := strings.ReplaceAll(removeRtfSyntax(record[11], results, ``), `\'`, `\x`)

		fmt.Printf("s1: %q\n", s1)
		fmt.Printf("s2: %q\n", s2)
		fmt.Printf("s3: %q\n", s3)
		fmt.Printf("s4: %q\n", s4)

		// big5 decoding
		content, _ := strconv.Unquote(`"` + fmt.Sprintf("%s", s3) + `"`)
		decoedStr, _, _ := transform.String(traditionalchinese.Big5.NewDecoder(), fmt.Sprintf("%s", content))
		fmt.Printf("decoedStr: %s\n", decoedStr)

		break
	}

	fmt.Println("end...")
	fmt.Printf("\n\n\n")

	testDecodeStr, _, _ := transform.String(traditionalchinese.Big5.NewDecoder(), "\xb7\x73\xab\xd8\xaf\x66\xbe\xfa")
	fmt.Printf("testDecodeStr: %s\n", testDecodeStr)

	fmt.Printf("str.q: %q\n", "\"string\"")
	fmt.Printf("str.s: %s\n", "\"string\"")

	fmt.Printf("golang.q: %q\n", "\"golang\"")
	fmt.Printf("golang.s: %s\n", "golang")
	// fmt.Printf("count: %d\n", count)
	// fmt.Printf("typeList: %+v\n", typeList)
	// fmt.Printf("varietyList.length: %d\n", len(varietyList))
	// fmt.Printf("varietyList: %v\n", strings.Join(varietyList, ","))
}
